## Objective

Create a standalone java application which allows users to manage their favourite recipes. It should
allow adding, updating, removing and fetching recipes. Additionally users should be able to filter
available recipes based on one or more of the following criteria:

1. Whether or not the dish is vegetarian
2. The number of servings
3. Specific ingredients (either include or exclude)
4. Text search within the instructions.
   For example, the API should be able to handle the following search requests:
   • All vegetarian recipes
   • Recipes that can serve 4 persons and have “potatoes” as an ingredient
   • Recipes without “salmon” as an ingredient that has “oven” in the instructions.

## Requirements

1. It must be a REST application implemented using Java (use a framework of your choice)
2. Your code should be production-ready.
3. REST API must be documented
4. Data must be persisted in a database
5. Unit tests must be present
6. Integration tests must be present

## Architectural choices

This application is written in Java 17 and the Springboot framework is applied.

The application consists of multiple layers:

1. Controller - This layer is responsible for receiving HTTP-requests and returning responses.
2. Service - This layer is responsible for the more complex business logic.
3. Repository - This layer is responsible for the communication with the database and performing CRUD operations.
4. Model - this layer contains all entities and DTO's.

For the simplicity of running the application H2 is used as the database technology.  
This is an in-memory database which means that no new local database has to be created in order to run this
application.  
This in turn does also mean that data is not persisted after closing the running application.

## Run the application

1. Clone application
2. Open the project in IDE of choice (IntelliJ/Eclipse)
3. Run the project
4. Open Swagger by opening URL: "http://localhost:8080/swagger-ui/index.html" in your browser
5. There are multiple different requests which are documented and can be executed from there.

## Using the API

1. When creating recipes, you can just copy/paste more IngredientAmounts to the request body if you want more than 1
   ingredient.
2. When searching for recipes through the "/api/recipes/search" request, you can use just 1 filter or combine multiple.
3. The examples mentioned in the assessment can be written as follows.
4. All vegetarian recipes: {"isVegetarian": "true"}
5. Recipes that can serve 4 persons and have “potatoes” as an ingredient: {"amountOfServings": 4, "
   includeIngredients": ["potatoes"]}
6. Recipes without “salmon” as an ingredient that has “oven” in the instructions: {"excludeIngredients": ["salmon"], "
   instructions": "oven"}
