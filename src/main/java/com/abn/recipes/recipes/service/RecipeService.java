package com.abn.recipes.recipes.service;

import com.abn.recipes.recipes.model.Ingredient;
import com.abn.recipes.recipes.model.MeasuringUnit;
import com.abn.recipes.recipes.model.Recipe;
import com.abn.recipes.recipes.model.dto.CreateRecipeDto;
import com.abn.recipes.recipes.model.dto.UpdateRecipeDto;
import com.abn.recipes.recipes.repository.IngredientRepository;
import com.abn.recipes.recipes.repository.RecipeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

@Service
public class RecipeService {
    private final RecipeRepository recipeRepository;
    private final IngredientRepository ingredientRepository;

    @Autowired
    RecipeService(RecipeRepository recipeRepository, IngredientRepository ingredientRepository) {
        this.recipeRepository = recipeRepository;
        this.ingredientRepository = ingredientRepository;
    }

    @Transactional(propagation = Propagation.REQUIRED, readOnly = true, noRollbackFor = Exception.class)
    public List<Recipe> findAll() {
        return recipeRepository.findAll();
    }

    public List<Recipe> findAll(Specification<Recipe> spec) {
        return recipeRepository.findAll(spec);
    }

    public Recipe create(CreateRecipeDto recipeDto) {
        Recipe recipe = new Recipe(recipeDto.getName(), recipeDto.getAmountOfServings(), recipeDto.getInstructions());

        recipeDto.getIngredientAmounts().forEach(ingredientAmount -> {
            Optional<Ingredient> ingredient = ingredientRepository.findByName(ingredientAmount.getIngredient().getName());

            if (ingredient.isEmpty()) {
                ingredient = Optional.of(ingredientRepository.save(ingredientAmount.getIngredient()));
            }

            recipe.addIngredientAmount(ingredient.get(), ingredientAmount.getAmount(), ingredientAmount.getUnit());
        });

        return recipeRepository.save(recipe);
    }

    public Recipe update(UpdateRecipeDto recipeDto) {
        Optional<Recipe> recipe = recipeRepository.findById(recipeDto.getId());

        if (recipe.isPresent()) {
            recipe.get().setName(recipeDto.getName());
            recipe.get().setAmountOfServings(recipeDto.getAmountOfServings());
            recipe.get().setInstructions(recipeDto.getInstructions());

            recipe.get().clearIngredientAmounts();
            recipeDto.getIngredientAmounts().forEach(ingredientAmount -> {
                        Optional<Ingredient> ingredient = ingredientRepository.findByName(ingredientAmount.getIngredient().getName());

                        if (ingredient.isEmpty()) {
                            ingredient = Optional.of(ingredientRepository.save(ingredientAmount.getIngredient()));
                        }
                        recipe.get().addIngredientAmount(ingredient.get(), ingredientAmount.getAmount(), ingredientAmount.getUnit());
                    }
            );

            return recipeRepository.save(recipe.get());
        }
        return null;
    }

    public void deleteById(Long id) {
        recipeRepository.deleteById(id);
    }


    /**
     * Method for creating some data which can be used to show the workings of the application.
     */
    public List<Recipe> createTestData() {
        Ingredient spaghettiPasta = new Ingredient("spaghetti pasta", true);
        Ingredient macaroniPasta = new Ingredient("macaroni pasta", true);
        Ingredient mincedBeef = new Ingredient("minced beef", false);
        Ingredient cheese = new Ingredient("cheese", true);
        Ingredient pastasauce = new Ingredient("pastasauce", true);
        Ingredient salmon = new Ingredient("salmon", false);


        Recipe traditionalSpaghetti = new Recipe("traditional spaghetti", 1, "1. kook de spaghetti. 2. bak het gehakt, 3. doe de saus bij het gehakt. 4. doe kaas op het gerecht.");
        Recipe vegetarianSpaghetti = new Recipe("vegetarian spaghetti", 1, "1. kook de spaghetti. 2. doe de saus bij het spaghetti. 3. doe kaas op het gerecht.");
        Recipe spaghettiWithoutCheese = new Recipe("spaghetti without cheese", 1, "1. kook de spaghetti. 2. doe de saus bij het spaghetti.");
        Recipe grilledSalmon = new Recipe("grilled salmon", 2, "1. Bak de zalm in een grillpan.");

        traditionalSpaghetti.addIngredientAmount(spaghettiPasta, BigDecimal.valueOf(100), MeasuringUnit.GRAM);
        traditionalSpaghetti.addIngredientAmount(cheese, BigDecimal.valueOf(100), MeasuringUnit.GRAM);
        traditionalSpaghetti.addIngredientAmount(mincedBeef, BigDecimal.valueOf(100), MeasuringUnit.GRAM);
        traditionalSpaghetti.addIngredientAmount(pastasauce, BigDecimal.valueOf(100), MeasuringUnit.GRAM);

        vegetarianSpaghetti.addIngredientAmount(spaghettiPasta, BigDecimal.valueOf(100), MeasuringUnit.GRAM);
        vegetarianSpaghetti.addIngredientAmount(cheese, BigDecimal.valueOf(100), MeasuringUnit.GRAM);
        vegetarianSpaghetti.addIngredientAmount(pastasauce, BigDecimal.valueOf(100), MeasuringUnit.GRAM);

        grilledSalmon.addIngredientAmount(salmon, BigDecimal.valueOf(1.5), MeasuringUnit.POUND);

        spaghettiWithoutCheese.addIngredientAmount(pastasauce, BigDecimal.valueOf(1.5), MeasuringUnit.POUND);
        spaghettiWithoutCheese.addIngredientAmount(spaghettiPasta, BigDecimal.valueOf(1.5), MeasuringUnit.POUND);

        ingredientRepository.saveAll(List.of(spaghettiPasta, macaroniPasta, mincedBeef, cheese, pastasauce, salmon));
        return recipeRepository.saveAll(List.of(traditionalSpaghetti, vegetarianSpaghetti, spaghettiWithoutCheese, grilledSalmon));
    }
}
