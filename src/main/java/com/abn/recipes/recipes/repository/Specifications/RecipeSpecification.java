package com.abn.recipes.recipes.repository.Specifications;

import com.abn.recipes.recipes.model.Ingredient;
import com.abn.recipes.recipes.model.IngredientAmount;
import com.abn.recipes.recipes.model.Recipe;
import com.abn.recipes.recipes.model.dto.RecipeSearch;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Class to combine SQL statements to a list of predicates which are handled by JPA to dynamically create and combine query's.
 */
public class RecipeSpecification implements Specification<Recipe> {
    private final RecipeSearch criteria;

    public RecipeSpecification(RecipeSearch ts) {
        criteria = ts;
    }

    @Override
    public Predicate toPredicate(Root<Recipe> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
        final List<Predicate> predicates = new ArrayList<>();

        if (criteria.getName() != null) {
            predicates.add(criteriaBuilder.like(root.get("name"), "%" + criteria.getName() + "%"));
        }

        if (criteria.getInstructions() != null) {
            predicates.add(criteriaBuilder.like(root.get("instructions"), "%" + criteria.getInstructions() + "%"));
        }

        if (criteria.getAmountOfServings() != null) {
            predicates.add(criteriaBuilder.equal(root.get("amountOfServings"), criteria.getAmountOfServings()));
        }

        if (criteria.getIncludeIngredients() != null && !criteria.getIncludeIngredients().equals(Collections.emptyList())) {
            // SQL Query for finding all recipes including the list of provided ingredients.
            //      SELECT r.*
            //      FROM   recipe r
            //      WHERE  (
            //          SELECT Count(*)
            //          FROM   ingredient i
            //          WHERE  i.NAME IN ( criteria.getIncludeIngredients() )) = (
            //              SELECT Count(*)
            //              FROM   ingredient_amount a
            //              INNER JOIN ingredient i
            //              ON i.id = a.ingredient_id
            //              WHERE  a.recipe_id = r.id
            //              AND i.NAME IN (criteria.getIncludeIngredients()));

            Subquery<Long> subquery1 = query.subquery(Long.class);
            Root<Ingredient> subquery1Ingredient = subquery1.from(Ingredient.class);
            subquery1.select(criteriaBuilder.count(subquery1Ingredient)).where(subquery1Ingredient.get("name").in(criteria.getIncludeIngredients()));

            Subquery<Long> subquery2 = subquery1.subquery(Long.class);
            Root<IngredientAmount> subquery2IngredientAmount = subquery2.from(IngredientAmount.class);
            Join<IngredientAmount, Ingredient> join = subquery2IngredientAmount.join("ingredient");
            Predicate p1 = criteriaBuilder.equal(root.get("id"), subquery2IngredientAmount.get("recipe"));
            Predicate p2 = join.get("name").in(criteria.getIncludeIngredients());
            subquery2.select(criteriaBuilder.count(subquery2IngredientAmount)).where(criteriaBuilder.and(p1, p2));

            predicates.add(criteriaBuilder.equal(subquery1, subquery2));
        }

        if (criteria.getExcludeIngredients() != null && !criteria.getExcludeIngredients().equals(Collections.emptyList())) {
            // SQL Query for finding all recipes excluding the list of provided ingredients.
            // SELECT r.*
            // FROM   recipe r
            // WHERE  NOT EXISTS (
            //              SELECT i.id
            //              FROM   ingredient_amount a
            //              INNER JOIN ingredient i
            //              ON i.id = a.ingredient_id
            //              AND r.id = a.recipe_id
            //              AND i.NAME IN ( criteria.getExcludeIngredients() ));

            Subquery<IngredientAmount> subQuery = query.subquery(IngredientAmount.class);
            Root<IngredientAmount> subqueryIngredientAmount = subQuery.from(IngredientAmount.class);
            subqueryIngredientAmount.join("recipe");
            subqueryIngredientAmount.join("ingredient");
            Predicate p1 = criteriaBuilder.equal(root.get("id"), subqueryIngredientAmount.get("recipe"));
            Predicate p2 = criteriaBuilder.isTrue(subqueryIngredientAmount.get("ingredient").get("name").in(criteria.getExcludeIngredients()));
            subQuery.select(subqueryIngredientAmount).where(criteriaBuilder.and(p1, p2));

            predicates.add(criteriaBuilder.exists(subQuery).not());
        }


        if (criteria.getIsVegetarian() != null && criteria.getIsVegetarian().isPresent()) {
            // SELECT r.*
            // FROM   recipe r
            // WHERE  (NOT) EXISTS(
            //              SELECT i.id
            //              FROM   ingredient_amount a
            //                     INNER JOIN ingredient i
            //                     ON i.id = a.ingredient_id
            //              WHERE  r.id = a.recipe_id
            //                     AND NOT i.is_vegetarian);

            Subquery<IngredientAmount> subQuery = query.subquery(IngredientAmount.class);
            Root<IngredientAmount> subqueryIngredientAmount = subQuery.from(IngredientAmount.class);
            Join<Object, Object> a = subqueryIngredientAmount.join("ingredient");
            Predicate p1 = criteriaBuilder.equal(root.get("id"), subqueryIngredientAmount.get("recipe"));
            Predicate p2 = criteriaBuilder.isFalse(a.get("isVegetarian"));

            subQuery.select(subqueryIngredientAmount).where(p1, p2);

            if (criteria.getIsVegetarian().get()) {
                predicates.add(criteriaBuilder.exists(subQuery).not());
            } else {
                predicates.add(criteriaBuilder.exists(subQuery));
            }
        }

        return criteriaBuilder.and(predicates.toArray(new Predicate[0]));
    }
}