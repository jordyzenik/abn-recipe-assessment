package com.abn.recipes.recipes.model;

import lombok.*;
import org.springframework.data.annotation.ReadOnlyProperty;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;


@Entity
@NoArgsConstructor
@RequiredArgsConstructor
@Getter
@Setter
public class Recipe {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false)
    private Long id;

    @NonNull
    private String name;
    @NonNull
    private int amountOfServings;

    @OneToMany(
            mappedBy = "recipe",
            cascade = CascadeType.ALL,
            orphanRemoval = true,
            fetch = FetchType.EAGER
    )

    private List<IngredientAmount> ingredientAmounts = new ArrayList<>();

    @ReadOnlyProperty
    @Getter(AccessLevel.NONE)
    @Transient
    private boolean isVegetarian;

    @NonNull
    private String instructions;

    public void addIngredientAmount(Ingredient ingredient, BigDecimal amount, MeasuringUnit measuringUnit) {
        IngredientAmount ingredientAmount = new IngredientAmount(this, ingredient, amount, measuringUnit);
        ingredientAmounts.add(ingredientAmount);
    }

    public void clearIngredientAmounts() {
        ingredientAmounts.clear();
    }

    public boolean isVegetarian() {
        return ingredientAmounts.stream().allMatch((ingredientAmount) -> ingredientAmount.getIngredient().isVegetarian());
    }
}
