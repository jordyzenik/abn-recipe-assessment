package com.abn.recipes.recipes.model.dto;

import lombok.Data;

import java.util.List;
import java.util.Optional;

@Data
public class RecipeSearch {
    private String name;
    private String instructions;
    private Integer amountOfServings;
    private List<String> includeIngredients;
    private List<String> excludeIngredients;
    private Optional<Boolean> isVegetarian;

}
