package com.abn.recipes.recipes.model;

public enum MeasuringUnit {
    MILLILITER,
    CENTILITER,
    LITER,
    FL_OZ,
    GRAM,
    OUNCE,
    POUND,
    KILO
}
