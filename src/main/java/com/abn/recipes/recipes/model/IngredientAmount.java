package com.abn.recipes.recipes.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@NoArgsConstructor
@Getter
@Setter
public class IngredientAmount {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false)
    private Long id;

    @ManyToOne(
            fetch = FetchType.LAZY
    )
    @JsonIgnore
    private Recipe recipe;

    @ManyToOne
    @JoinColumn(name = "ingredient_id")
    @NonNull
    private Ingredient ingredient;

    @NonNull
    private BigDecimal amount;

    @NonNull
    private MeasuringUnit unit;

    public IngredientAmount(Recipe recipe, @NonNull Ingredient ingredient, @NonNull BigDecimal amount, @NonNull MeasuringUnit unit) {
        this.recipe = recipe;
        this.ingredient = ingredient;
        this.amount = amount;
        this.unit = unit;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        IngredientAmount that = (IngredientAmount) o;

        if (!ingredient.equals(that.ingredient)) return false;
        if (amount.compareTo(that.amount) != 0) return false;
        return unit == that.unit;
    }

    @Override
    public int hashCode() {
        int result = ingredient.hashCode();
        result = 31 * result + amount.hashCode();
        result = 31 * result + unit.hashCode();
        return result;
    }
}
