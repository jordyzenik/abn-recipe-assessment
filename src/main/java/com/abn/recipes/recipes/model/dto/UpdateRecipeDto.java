package com.abn.recipes.recipes.model.dto;

import com.abn.recipes.recipes.model.IngredientAmount;
import lombok.*;

import java.util.List;

@Data
@Getter
@Setter
@RequiredArgsConstructor
@ToString
public class UpdateRecipeDto {
    @NonNull
    private long id;

    @NonNull
    private String name;

    @NonNull
    private int amountOfServings;

    @NonNull
    private List<IngredientAmount> ingredientAmounts;

    @NonNull
    private String instructions;
}
