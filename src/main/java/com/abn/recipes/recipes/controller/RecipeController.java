package com.abn.recipes.recipes.controller;

import com.abn.recipes.recipes.model.Recipe;
import com.abn.recipes.recipes.model.dto.CreateRecipeDto;
import com.abn.recipes.recipes.model.dto.RecipeSearch;
import com.abn.recipes.recipes.model.dto.UpdateRecipeDto;
import com.abn.recipes.recipes.repository.Specifications.RecipeSpecification;
import com.abn.recipes.recipes.service.RecipeService;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/recipes")
public class RecipeController {

    private final RecipeService recipeService;

    @Autowired
    RecipeController(RecipeService recipeService) {
        this.recipeService = recipeService;
    }

    @Operation(summary = "Finds all recipes.")
    @GetMapping
    public List<Recipe> findAll() {
        return recipeService.findAll();
    }


    @Operation(summary = "Finds all recipes which meet the conditions specified in the search Object. Not all properties of the object have to be used and they can all be combined.")
    @PostMapping("/search")
    public List<Recipe> findAll(@RequestBody RecipeSearch search) {
        Specification<Recipe> spec = new RecipeSpecification(search);
        return recipeService.findAll(spec);
    }

    @Operation(summary = "Creates and saves a new recipe. Possible values for unit in an ingredient are MILLILITER, CENTILITER, LITER, FL_OZ, GRAM, OUNCE, POUND, KILO")
    @PostMapping
    public Recipe create(@RequestBody CreateRecipeDto recipe) {
        return recipeService.create(recipe);
    }

    @Operation(summary = "Updates an existing recipe.")
    @PutMapping
    public Recipe update(@RequestBody UpdateRecipeDto recipe) {
        return recipeService.update(recipe);
    }

    @Operation(summary = "Deletes an existing recipe.")
    @DeleteMapping("/{id}")
    public void delete(@PathVariable Long id) {
        recipeService.deleteById(id);
    }

    @Operation(summary = "Creates 4 new recipes.")
    @PostMapping("/create")
    public List<Recipe> createTestData() {
        return recipeService.createTestData();
    }

}
