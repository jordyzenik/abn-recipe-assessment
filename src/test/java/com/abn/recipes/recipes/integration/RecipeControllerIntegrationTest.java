package com.abn.recipes.recipes.integration;

import com.abn.recipes.recipes.model.Ingredient;
import com.abn.recipes.recipes.model.MeasuringUnit;
import com.abn.recipes.recipes.model.Recipe;
import com.abn.recipes.recipes.model.dto.RecipeSearch;
import com.abn.recipes.recipes.repository.IngredientRepository;
import com.abn.recipes.recipes.repository.RecipeRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.math.BigDecimal;
import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
public class RecipeControllerIntegrationTest {

    @Autowired
    private MockMvc mvc;

    @Autowired
    private RecipeRepository recipeRepository;

    @Autowired
    private IngredientRepository ingredientRepository;

    @Test
    public void testFindAllRecipes() throws Exception {
        // GIVEN: there is 1 recipe
        createRecipe(true);

        // WHEN request is performed THEN expect the following JSON as return
        mvc.perform(MockMvcRequestBuilders
                        .get("/api/recipes")
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$").isArray())
                .andExpect(MockMvcResultMatchers.jsonPath("$", hasSize(1)))
                .andExpect(MockMvcResultMatchers.jsonPath("$[*].id").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$[*].name").value("traditional spaghetti"))
                .andExpect(MockMvcResultMatchers.jsonPath("$[*].amountOfServings").value(1))
                .andExpect(MockMvcResultMatchers.jsonPath("$[*].instructions").value("1. kook de spaghetti. 2. bak het gehakt, 3. doe de saus bij het gehakt. 4. doe kaas op het gerecht."))
                .andExpect(MockMvcResultMatchers.jsonPath("$[*].ingredientAmounts").isArray())
                .andExpect(MockMvcResultMatchers.jsonPath("$[*].ingredientAmounts[*].ingredient").isNotEmpty())
                .andExpect(MockMvcResultMatchers.jsonPath("$[*].ingredientAmounts[*].amount").isNotEmpty())
                .andExpect(MockMvcResultMatchers.jsonPath("$[*].ingredientAmounts[*].unit").isNotEmpty());

        // AND WHEN another recipe is saved and the request is performed again
        createRecipe(true);

        // THEN expect a list of 2 recipes
        mvc.perform(MockMvcRequestBuilders
                        .get("/api/recipes")
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$").isArray())
                .andExpect(MockMvcResultMatchers.jsonPath("$", hasSize(2)));
    }

    @Test
    public void testFindAllRecipesByName() throws Exception {
        // GIVEN: there are 4 recipes
        createListOfRecipes();

        RecipeSearch searchSpecification = new RecipeSearch();
        searchSpecification.setName("salmon");

        // WHEN request is performed with a search for the name "salmon" THEN expect 1 recipe in return
        mvc.perform(MockMvcRequestBuilders
                        .post("/api/recipes/search")
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON).content(asJsonString(searchSpecification)))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$").isArray())
                .andExpect(MockMvcResultMatchers.jsonPath("$", hasSize(1)))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].id").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].name").value("grilled salmon"));
    }

    @Test
    public void testFindAllRecipesByInstructions() throws Exception {
        // GIVEN: there are 4 recipes
        createListOfRecipes();

        RecipeSearch searchSpecification = new RecipeSearch();
        searchSpecification.setInstructions("grill");

        // WHEN request is performed with a search for the instructions "grill" THEN expect 1 recipe in return
        mvc.perform(MockMvcRequestBuilders
                        .post("/api/recipes/search")
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON).content(asJsonString(searchSpecification)))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$").isArray())
                .andExpect(MockMvcResultMatchers.jsonPath("$", hasSize(1)))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].id").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].name").value("grilled salmon"));
    }

    @Test
    public void testFindAllRecipesByAmountOfServings() throws Exception {
        // GIVEN: there are 4 recipes
        createListOfRecipes();

        RecipeSearch searchSpecification = new RecipeSearch();
        searchSpecification.setAmountOfServings(2);

        // WHEN request is performed with a search for the amount of servings "2" THEN expect 1 recipe in return
        mvc.perform(MockMvcRequestBuilders
                        .post("/api/recipes/search")
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON).content(asJsonString(searchSpecification)))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$").isArray())
                .andExpect(MockMvcResultMatchers.jsonPath("$", hasSize(1)))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].id").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].name").value("grilled salmon"));
    }

    @Test
    public void testFindAllRecipesByIncludeIngredients() throws Exception {
        // GIVEN: there are 4 recipes
        createListOfRecipes();

        RecipeSearch searchSpecificationSingleIngredient = new RecipeSearch();
        searchSpecificationSingleIngredient.setIncludeIngredients(List.of("cheese"));

        RecipeSearch searchSpecificationMultipleIngredients = new RecipeSearch();
        searchSpecificationMultipleIngredients.setIncludeIngredients(List.of("cheese", "minced beef"));

        // WHEN request is performed with a search for the includeIngredients "cheese" THEN expect 2 recipes in return
        mvc.perform(MockMvcRequestBuilders
                        .post("/api/recipes/search")
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON).content(asJsonString(searchSpecificationSingleIngredient)))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$").isArray())
                .andExpect(MockMvcResultMatchers.jsonPath("$", hasSize(2)))
                .andExpect(MockMvcResultMatchers.jsonPath("$[*].id").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].name", is("traditional spaghetti")))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].name", is("vegetarian spaghetti")));

        // WHEN request is performed with a search for the includeIngredients "cheese" and "minced beef" THEN expect 1 recipe in return
        mvc.perform(MockMvcRequestBuilders
                        .post("/api/recipes/search")
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON).content(asJsonString(searchSpecificationMultipleIngredients)))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$").isArray())
                .andExpect(MockMvcResultMatchers.jsonPath("$", hasSize(1)))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].id").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].name").value("traditional spaghetti"));
    }

    @Test
    public void testFindAllRecipesByExcludeIngredients() throws Exception {
        // GIVEN: there are 4 recipes
        createListOfRecipes();

        RecipeSearch searchSpecificationSingleIngredient = new RecipeSearch();
        searchSpecificationSingleIngredient.setExcludeIngredients(List.of("spaghetti pasta"));

        RecipeSearch searchSpecificationMultipleIngredients = new RecipeSearch();
        searchSpecificationMultipleIngredients.setExcludeIngredients(List.of("cheese", "minced beef", "salmon"));

        // WHEN request is performed with a search for the excludeIngredients "spaghetti pasta" THEN expect 1 recipes in return
        mvc.perform(MockMvcRequestBuilders
                        .post("/api/recipes/search")
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON).content(asJsonString(searchSpecificationSingleIngredient)))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$").isArray())
                .andExpect(MockMvcResultMatchers.jsonPath("$", hasSize(1)))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].id").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].name").value("grilled salmon"));


        // WHEN request is performed with a search for the excludeIngredients "salmon", "cheese" and "minced beef" THEN expect 1 recipe in return
        mvc.perform(MockMvcRequestBuilders
                        .post("/api/recipes/search")
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON).content(asJsonString(searchSpecificationMultipleIngredients)))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$").isArray())
                .andExpect(MockMvcResultMatchers.jsonPath("$", hasSize(1)))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].id").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].name").value("vegetarian spaghetti without cheese"));
    }

    @Test
    public void testFindAllRecipesByIsVegetarianTrue() throws Exception {
        // GIVEN: there are 4 recipes
        createListOfRecipes();

        // WHEN request is performed with a search for the isVegetarian true THEN expect 2 recipes in return
        // JSON string is used here because mockmvc does not know how to properly translate the optional<boolean>
        mvc.perform(MockMvcRequestBuilders
                        .post("/api/recipes/search")
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON).content("{\"isVegetarian\": true}"))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$").isArray())
                .andExpect(MockMvcResultMatchers.jsonPath("$", hasSize(2)))
                .andExpect(MockMvcResultMatchers.jsonPath("$[*].id").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].name", is("vegetarian spaghetti")))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].name", is("vegetarian spaghetti without cheese")));
    }

    @Test
    public void testFindAllRecipesByIsVegetarianFalse() throws Exception {
        // GIVEN: there are 4 recipes
        createListOfRecipes();

        // WHEN request is performed with a search for the isVegetarian true THEN expect 2 recipes in return
        // JSON string is used here because mockmvc does not know how to properly translate the optional<boolean>
        mvc.perform(MockMvcRequestBuilders
                        .post("/api/recipes/search")
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON).content("{\"isVegetarian\": false}"))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$").isArray())
                .andExpect(MockMvcResultMatchers.jsonPath("$", hasSize(2)))
                .andExpect(MockMvcResultMatchers.jsonPath("$[*].id").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].name", is("traditional spaghetti")))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].name", is("grilled salmon")));
    }

    @Test
    public void testFindAllRecipesByAllProperties() throws Exception {
        // GIVEN: there are 4 recipes
        createListOfRecipes();

        RecipeSearch searchSpecification = new RecipeSearch();
        searchSpecification.setName("spaghetti");
        searchSpecification.setInstructions("kook");
        searchSpecification.setAmountOfServings(1);
        searchSpecification.setIncludeIngredients(List.of("spaghetti pasta"));
        searchSpecification.setExcludeIngredients(List.of("cheese", "minced beef"));

        // WHEN request is performed with a search for the name "salmon" THEN expect 1 recipe in return
        mvc.perform(MockMvcRequestBuilders
                        .post("/api/recipes/search")
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON).content(asJsonString(searchSpecification)))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$").isArray())
                .andExpect(MockMvcResultMatchers.jsonPath("$", hasSize(1)))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].id").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].name").value("vegetarian spaghetti without cheese"));
    }

    @Test
    public void testCreateRecipe() throws Exception {
        // WHEN a request for saving a recipe is performed THEN expect a response with the recipe in JSON
        mvc.perform(MockMvcRequestBuilders.post("/api/recipes")
                        .content(asJsonString(createRecipe(false)))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$.name").value("traditional spaghetti"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.amountOfServings").value(1))
                .andExpect(MockMvcResultMatchers.jsonPath("$.instructions").value("1. kook de spaghetti. 2. bak het gehakt, 3. doe de saus bij het gehakt. 4. doe kaas op het gerecht."))
                .andExpect(MockMvcResultMatchers.jsonPath("$.ingredientAmounts").isArray())
                .andExpect(MockMvcResultMatchers.jsonPath("$.ingredientAmounts[*].ingredient").isNotEmpty())
                .andExpect(MockMvcResultMatchers.jsonPath("$.ingredientAmounts[*].amount").isNotEmpty())
                .andExpect(MockMvcResultMatchers.jsonPath("$.ingredientAmounts[*].unit").isNotEmpty());

        // AND THEN the recipe is saved
        Assertions.assertEquals(recipeRepository.findAll().size(), 1);
    }

    @Test
    public void testUpdateRecipe() throws Exception {
        // GIVEN there is a saved recipe
        Ingredient newIngredient = new Ingredient("spaghetti pasta2", true);
        Recipe recipe = createRecipe(true);

        // WHEN the values of the recipe are changed and a update request is performed THEN expect a response with the recipe in JSON
        recipe.setName("traditional spaghetti2");
        recipe.setAmountOfServings(2);
        recipe.setInstructions("new instructions");
        recipe.clearIngredientAmounts();
        recipe.addIngredientAmount(newIngredient, BigDecimal.valueOf(500), MeasuringUnit.POUND);

        mvc.perform(MockMvcRequestBuilders.put("/api/recipes")
                        .content(asJsonString(recipe))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$.name").value("traditional spaghetti2"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.amountOfServings").value(2))
                .andExpect(MockMvcResultMatchers.jsonPath("$.instructions").value("new instructions"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.ingredientAmounts").isArray())
                .andExpect(MockMvcResultMatchers.jsonPath("$.ingredientAmounts[*].ingredient.name").value("spaghetti pasta2"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.ingredientAmounts[*].amount").value(500))
                .andExpect(MockMvcResultMatchers.jsonPath("$.ingredientAmounts[*].unit").value("POUND"));

        // AND THEN the recipe is updated in the database
        Assertions.assertEquals(1, recipeRepository.findAll().size());
        Assertions.assertEquals(recipe.getName(), recipeRepository.findAll().get(0).getName());
        Assertions.assertEquals(recipe.getAmountOfServings(), recipeRepository.findAll().get(0).getAmountOfServings());
        Assertions.assertEquals(recipe.getInstructions(), recipeRepository.findAll().get(0).getInstructions());
        Assertions.assertEquals(1, recipeRepository.findAll().get(0).getIngredientAmounts().size());
        Assertions.assertEquals(newIngredient, recipeRepository.findAll().get(0).getIngredientAmounts().get(0).getIngredient());
        Assertions.assertEquals(0, recipeRepository.findAll().get(0).getIngredientAmounts().get(0).getAmount().compareTo(recipe.getIngredientAmounts().get(0).getAmount()));
        Assertions.assertEquals(recipe.getIngredientAmounts().get(0).getUnit(), recipeRepository.findAll().get(0).getIngredientAmounts().get(0).getUnit());
    }

    @Test
    public void testDeleteRecipe() throws Exception {
        Recipe recipe = createRecipe(true);
        Assertions.assertEquals(recipeRepository.findAll().size(), 1);

        mvc.perform(MockMvcRequestBuilders.delete("/api/recipes/{id}", recipe.getId()))
                .andExpect(status().isOk());

        Assertions.assertEquals(recipeRepository.findAll().size(), 0);
    }

    private Recipe createRecipe(boolean saveObjectInDatabase) {
        Ingredient spaghettiPasta = new Ingredient("spaghetti pasta", true);
        Ingredient mincedBeef = new Ingredient("minced beef", false);
        Ingredient cheese = new Ingredient("cheese", true);
        Ingredient pastasauce = new Ingredient("pastasauce", true);


        Recipe traditionalSpaghetti = new Recipe("traditional spaghetti", 1, "1. kook de spaghetti. 2. bak het gehakt, 3. doe de saus bij het gehakt. 4. doe kaas op het gerecht.");

        traditionalSpaghetti.addIngredientAmount(spaghettiPasta, BigDecimal.valueOf(100), MeasuringUnit.GRAM);
        traditionalSpaghetti.addIngredientAmount(cheese, BigDecimal.valueOf(100), MeasuringUnit.GRAM);
        traditionalSpaghetti.addIngredientAmount(mincedBeef, BigDecimal.valueOf(100), MeasuringUnit.GRAM);
        traditionalSpaghetti.addIngredientAmount(pastasauce, BigDecimal.valueOf(100), MeasuringUnit.GRAM);

        if (saveObjectInDatabase) {
            ingredientRepository.saveAll(List.of(spaghettiPasta, mincedBeef, cheese, pastasauce));
            return recipeRepository.save(traditionalSpaghetti);
        }

        return traditionalSpaghetti;
    }

    private List<Recipe> createListOfRecipes() {
        Ingredient spaghettiPasta = new Ingredient("spaghetti pasta", true);
        Ingredient mincedBeef = new Ingredient("minced beef", false);
        Ingredient cheese = new Ingredient("cheese", true);
        Ingredient pastasauce = new Ingredient("pastasauce", true);
        Ingredient salmon = new Ingredient("salmon", false);


        Recipe traditionalSpaghetti = new Recipe("traditional spaghetti", 1, "1. kook de spaghetti. 2. bak het gehakt, 3. doe de saus bij het gehakt. 4. doe kaas op het gerecht.");
        Recipe vegetarianSpaghetti = new Recipe("vegetarian spaghetti", 1, "1. kook de spaghetti. 2. doe de saus bij het spaghetti. 3. doe kaas op het gerecht.");
        Recipe vegetarianSpaghettiWithoutCheese = new Recipe("vegetarian spaghetti without cheese", 1, "1. kook de spaghetti. 2. doe de saus bij het spaghetti.");
        Recipe grilledSalmon = new Recipe("grilled salmon", 2, "1. Bak de zalm in een grillpan.");

        traditionalSpaghetti.addIngredientAmount(spaghettiPasta, BigDecimal.valueOf(100), MeasuringUnit.GRAM);
        traditionalSpaghetti.addIngredientAmount(cheese, BigDecimal.valueOf(100), MeasuringUnit.GRAM);
        traditionalSpaghetti.addIngredientAmount(mincedBeef, BigDecimal.valueOf(100), MeasuringUnit.GRAM);
        traditionalSpaghetti.addIngredientAmount(pastasauce, BigDecimal.valueOf(100), MeasuringUnit.GRAM);

        vegetarianSpaghetti.addIngredientAmount(spaghettiPasta, BigDecimal.valueOf(100), MeasuringUnit.GRAM);
        vegetarianSpaghetti.addIngredientAmount(cheese, BigDecimal.valueOf(100), MeasuringUnit.GRAM);
        vegetarianSpaghetti.addIngredientAmount(pastasauce, BigDecimal.valueOf(100), MeasuringUnit.GRAM);

        grilledSalmon.addIngredientAmount(salmon, BigDecimal.valueOf(1.5), MeasuringUnit.POUND);

        vegetarianSpaghettiWithoutCheese.addIngredientAmount(pastasauce, BigDecimal.valueOf(1.5), MeasuringUnit.POUND);
        vegetarianSpaghettiWithoutCheese.addIngredientAmount(spaghettiPasta, BigDecimal.valueOf(1.5), MeasuringUnit.POUND);

        ingredientRepository.saveAll(List.of(spaghettiPasta, mincedBeef, cheese, pastasauce, salmon));
        return recipeRepository.saveAll(List.of(traditionalSpaghetti, vegetarianSpaghetti, vegetarianSpaghettiWithoutCheese, grilledSalmon));
    }

    private static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
